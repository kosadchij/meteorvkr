// Register your apis here

import '../../api/links/methods.js';
import '../../api/users/server/methods.js';
import '../../api/companies/server/methods.js';
import '../../api/containers/server/methods.js';
import '../../api/containers/server/publications.js';
import '../../api/users/server/publications.js';
import '../../api/companies/server/publications.js';
import '../../api/log/server/publications.js';
import '../../api/links/server/publications.js';
