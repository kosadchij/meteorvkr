import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import {Tracker} from 'meteor/tracker';

// Import needed templates
import '../../ui/layouts/body/body.js';
import '../../ui/layouts/blank/blank.js';
import '../../ui/pages/home/home.js';
import '../../ui/pages/not-found/not-found.js';
import '../../ui/pages/login/login.js';
import '../../ui/pages/companies/companies-list.js';
import '../../ui/pages/companies/companies-view.js';
import '../../ui/pages/containers/containers-add.js';
import '../../ui/pages/containers/containers-update.js';
import '../../ui/pages/containers/containers-list-all.js';
import '../../ui/pages/containers/containers-list-company.js';
import '../../ui/pages/containers/containers-view.js';
import '../../ui/pages/users/users-list.js';
import '../../ui/pages/users/users-view.js';
import '../../ui/pages/users/users-add.js';
import '../../ui/pages/users/users-update.js';
import '../../ui/pages/register/register.js';
import '../../ui/pages/log/log-page.js';

FlowRouter.wait();

// Set up all routes in the app

let allUsers = FlowRouter.group({});
let managerUsers = FlowRouter.group({
    triggersEnter: [function (context, redirect) {
        if(!Meteor.loggingIn() && !Meteor.userId())
            FlowRouter.go('App.login');
    }]
});
let adminUsers = FlowRouter.group({});

managerUsers.route('/', {
  name: 'App.home',
  action() {
    BlazeLayout.render('App_body', { content: 'App_home' });
  },
});

managerUsers.route('/users/view/:uId', {
    name: 'App.usersView',
    action: function() {
        BlazeLayout.render("App_body", {content: "usersView"});
    }
});

managerUsers.route('/companies/view/:cId', {
    name: 'App.companiesView',
    action: function() {
        BlazeLayout.render("App_body", {content: "companiesView"});
    }
});

managerUsers.route('/companies/list', {
    name: 'App.companiesList',
    action: function() {
        BlazeLayout.render("App_body", {content: "companiesList"});
    }
});

managerUsers.route('/containers/add', {
    name: 'App.containersAdd',
    action: function() {
        BlazeLayout.render("App_body", {content: "containersAdd"});
    }
});

managerUsers.route('/containers/update/:conId', {
    name: 'App.containersUpdate',
    action: function() {
        BlazeLayout.render("App_body", {content: "containersUpdate"});
    }
});

allUsers.notFound = {
  action() {
    BlazeLayout.render('App_body', { content: 'App_notFound' });
  },
};

allUsers.route('/login', {
    name: 'App.login',
    action: function() {
        BlazeLayout.render("blankLayout", {content: "login"});
    }
});

allUsers.route('/register', {
    name: 'App.register',
    action: function() {
        BlazeLayout.render("blankLayout", {content: "register"});
    }
});

allUsers.route('/containers/list-all', {
    name: 'App.containersListAll',
    action: function() {
        BlazeLayout.render("App_body", {content: "containersListAll"});
    }
});

allUsers.route('/containers/list-company', {
    name: 'App.containersListCompany',
    action: function() {
        BlazeLayout.render("App_body", {content: "containersListCompany"});
    }
});

allUsers.route('/containers/view/:conId', {
    name: 'App.containersView',
    action: function() {
        BlazeLayout.render("App_body", {content: "containersView"});
    }
});

adminUsers.route('/users/list', {
    name: 'App.usersList',
    action: function() {
        BlazeLayout.render("App_body", {content: "usersList"});
    }
});

adminUsers.route('/users/add', {
    name: 'App.usersAdd',
    action: function() {
        BlazeLayout.render("App_body", {content: "usersAdd"});
    }
});

adminUsers.route('/users/update/:uId', {
    name: 'App.usersUpdate',
    action: function() {
        BlazeLayout.render("App_body", {content: "usersUpdate"});
    }
});

adminUsers.route('/log', {
    name: 'App.log',
    action: function() {
        BlazeLayout.render("App_body", {content: "logPage"});
    }
});

Tracker.autorun(function(){
    if (Roles.subscription.ready()){
        FlowRouter.initialize();
    }
})
