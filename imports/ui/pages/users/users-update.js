import './users-update.html';
import {Template} from 'meteor/templating';
import '../../components/users/users-update-form-com.js';
import '../../components/common/page-heading/page-heading.js';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.usersUpdate.helpers({
    getUserId(){
        return FlowRouter.getParam('uId');
    },
    getLinks(){
        return [{
        		label: 'Home',
        		url: FlowRouter.path('App.home')
        	},
        	{
        		label: 'User profile update'
        	}
        ];
    },
});
