import './users-view.html';
import {Template} from 'meteor/templating';
import '../../components/users/users-view-com.js';
import '../../components/common/page-heading/page-heading.js';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.usersView.helpers({
    getUserId(){
        return FlowRouter.getParam('uId');
    },
    getLinks(){
        return [{
        		label: 'Home',
        		url: FlowRouter.path('App.home')
        	},
        	{
        		label: 'User profile'
        	}
        ];
    },
});
