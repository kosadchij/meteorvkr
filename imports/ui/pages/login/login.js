import './login.html';
import {Template} from 'meteor/templating';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {TAPi18n} from 'meteor/tap:i18n';

Template.login.onCreated(function () {
    if(Meteor.loggingIn() || Meteor.userId())
        FlowRouter.go('App.home');
});

Template.login.events({
    'click #jsLogin': function(e) {
        let login = $('#loginField').val();
        let password = $('#passwordField').val();
        Meteor.loginWithPassword(login, password, function(e) {
            if (e){
                Command: toastr['error'](TAPi18n.__(e.reason), 'Authentication error');
                console.log(e.reason);
            } else {
                FlowRouter.go('App.home');
            }
        });
    }
})
