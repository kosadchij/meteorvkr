import './companies-view.html';
import {Template} from 'meteor/templating';
import '../../components/companies/companies-view-com.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import '../../components/common/page-heading/page-heading.js';

Template.companiesView.helpers({
    getCompanyId(){
        return FlowRouter.getParam('cId');
    },
    getLinks(){
        return [{
        		label: 'Home',
        		url: FlowRouter.path('App.home')
        	},
        	{
        		label: 'Company info'
        	}
        ];
    }
});
