import './register.html';
import {Template} from 'meteor/templating';
import '../../components/companies/register-company-form.js';

Template.register.onCreated(function () {
    if(Meteor.loggingIn() || Meteor.userId())
        FlowRouter.go('App.home');
});
