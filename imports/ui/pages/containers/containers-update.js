import './containers-update.html';
import {Template} from 'meteor/templating';
import '../../components/containers/containers-update-com.js';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.containersUpdate.helpers({
    getContainerId(){
        return FlowRouter.getParam('conId');
    }
});
