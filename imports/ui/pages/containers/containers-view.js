import './containers-view.html';
import {Template} from 'meteor/templating';
import '../../components/containers/containers-view-com.js';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.containersView.helpers({
    getContainerId(){
        return FlowRouter.getParam('conId');
    }
});
