import './log-page.html';
import {Template} from 'meteor/templating';
import '../../components/common/log/log-list.js';
import '../../components/common/page-heading/page-heading.js';
import { FlowRouter } from 'meteor/kadira:flow-router';
import {moment} from 'meteor/momentjs:moment';


import {ReactiveVar} from 'meteor/reactive-var';

Template.logPage.onCreated(function () {
    this.selectedDate = new ReactiveVar(moment().format('YYYY-MM-DD'));
});

Template.logPage.helpers({
    getLinks(){
        return [{
        		label: 'Home',
        		url: FlowRouter.path('App.home')
        	},
        	{
        		label: 'Log'
        	}
        ];
    },
    getDate(){
        return moment().format('YYYY-MM-DD');
    },
    getSelectedDate(){
        return Template.instance().selectedDate;
    }
});

Template.logPage.onRendered(function () {
    let templateInstance = Template.instance();
    $('.input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    })
});

Template.logPage.events({
    'change #filterDate': function(e, template) {
        let date = $('#filterDate').val();
        template.selectedDate.set(moment(date).format('YYYY-MM-DD'));
    }
});
