import './users-update-form-com.html';
import '../common/autoforms/schema.js';
import { Meteor } from 'meteor/meteor';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {Template} from 'meteor/templating';

Template.updateUserForm.onCreated(function () {
    this.subscribe('users.one', this.data.userId);
});

Template.updateUserForm.helpers({
    getUserId(){
        return this.userId;
    },
    updateUserFormSchema(){
      return Schema.updateUser;
    },
    getUserInfo() {
		let user = Meteor.users.findOne({_id: this.userId});
        if(user != 'undefined'){
            return {
                userId: user._id,
                name: user.profile.name,
                email: user.emails[0].address,
                workPhone: user.profile.workPhone,
                phone: user.profile.phone,
                position: user.profile.position,
                office: user.profile.office,
                skype: user.profile.skype,
                otherInfo: user.profile.otherInfo
            };
        }
	},
});

Template.updateUserForm.onRendered(function() {
	let updateUserFormHooks = {

		// Called when any submit operation succeeds
		onSuccess: function(formType, result) {
			toastr['success'](TAPi18n.__('User successfully updated'), 'Success');
			FlowRouter.go('/users/view/' + FlowRouter.getParam('uId'));
		},

		// Called when any submit operation fails
		onError: function(formType, error) {
			toastr['error'](error.reason, 'Error');
		},
	};
	AutoForm.addHooks('updateUserForm', updateUserFormHooks, true)
});
