import './users-view-com.html';
import {Template} from 'meteor/templating';
import {Mongo} from 'meteor/mongo';
import {Companies} from '../../../api/companies/index.js';
import {ProfileImages} from '../../../api/users/index.js';

Template.usersViewCom.onCreated(function() {
	this.subscribe('users.one', this.data.userId);
});

Template.usersViewCom.helpers({
	getUserId() {
		return this.userId;
	},
	getUserInfo() {
		let userInfo = [];
		let user = Meteor.users.findOne(this.userId);
		return {
			name: user.profile.name,
			email: user.emails[0].address,
			phone: user.profile.phone,
			workPhone: user.profile.workPhone,
			position: user.profile.position,
			office: user.profile.office,
			skype: user.profile.skype,
			otherInfo: user.profile.otherInfo,
			profileImage: user.profile.profileImage,
			companyName: Companies.findOne({
				_id: user.profile.company
			}).name
		};
	}
});
