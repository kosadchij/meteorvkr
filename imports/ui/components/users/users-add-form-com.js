import './users-add-form-com.html';
import '../common/autoforms/schema.js';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {Template} from 'meteor/templating';

Template.addUserForm.helpers({
  addUserFormSchema: function() {
    return Schema.addUser;
  }
});

Template.addUserForm.onRendered(function() {
	let addUserFormHooks = {

		// Called when any submit operation succeeds
		onSuccess: function(formType, result) {
			toastr['success'](TAPi18n.__('User successfully created'), 'Success');
			FlowRouter.go('App.usersList');
		},

		// Called when any submit operation fails
		onError: function(formType, error) {
			toastr['error'](error.reason, 'Error');
		},
	};
	AutoForm.addHooks('addUserForm', addUserFormHooks, true)
});
