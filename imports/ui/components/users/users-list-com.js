import './users-list-com.html';
import { Meteor } from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {Companies} from '../../../api/companies/index.js';
import {ProfileImages} from '../../../api/users/index.js';

Template.usersListCom.onCreated(function () {
    this.subscribe('users.all');
});

Template.usersListCom.helpers({
    users() {
        return Meteor.users.find();
    },
    getEmail(){
        return this.emails[0].address;
    },
    getUserRoles(){
        if (this.roles != 'undefined'){
            return this.roles.map(function(role) {
                return role;
            }).join(', ');
        }
    },
    getUserCompany(){
        return Companies.findOne({
            _id: this.profile.company
        }).name;
    },
    getUserInfo() {
		return {
			name: this.profile.name,
			email: this.emails[0].address,
			phone: this.profile.phone,
			workPhone: this.profile.workPhone,
			position: this.profile.position,
			office: this.profile.office,
			skype: this.profile.skype,
			otherInfo: this.profile.otherInfo,
			profileImage: this.profile.profileImage,
			companyName: Companies.findOne({
				_id: this.profile.company
			}).name
		};
	},
});
