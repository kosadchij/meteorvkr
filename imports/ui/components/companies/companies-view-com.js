import './companies-view-com.html';
import {Template} from 'meteor/templating';
import {Mongo} from 'meteor/mongo';
import {Companies} from '../../../api/companies/index.js';
import {Containers} from '../../../api/containers/index.js';

Template.companiesViewCom.onCreated(function() {
	this.subscribe('companies.one', this.data.companyId);
});

Template.companiesViewCom.helpers({
	getCompanyId() {
		return this.companyId;
	},
	getCompanyInfo() {
		let companyInfo = [];
		let company = Companies.findOne({_id: this.companyId});
		return {
			name: company.name,
			location: company.location,
			INN: company.INN,
			KPP: company.KPP,
		};
	},
    users() {
        return Meteor.users.find();
    },
    containers() {
        return Containers.find();
    },
    getEmail(){
        return this.emails[0].address;
    },
    getUserInfo() {
		return {
			name: this.profile.name,
			email: this.emails[0].address,
			phone: this.profile.phone,
			workPhone: this.profile.workPhone,
			position: this.profile.position,
			office: this.profile.office,
			skype: this.profile.skype,
			otherInfo: this.profile.otherInfo,
			profileImage: this.profile.profileImage,
			companyName: Companies.findOne({
				_id: this.profile.company
			}).name
		};
	},
});
/*
Template.usersViewCom.events({
	'change #profileimg': function() {
		let input = $('#profileimg');
		if (input.prop('files') && input[0].files[0]) {
			let reader = new FileReader();

			reader.onload = function(e) {
				$('#blah').attr('src', e.target.result);
				$crop = $('#blah').croppie({
					enableExif: true,
					viewport: {
						width: 200,
						height: 200,
						type: 'circle'
					},
					boundary: {
						width: 300,
						height: 300
					}
				});
			}

			reader.readAsDataURL(input[0].files[0]);
		}
	},
	'click #uploadPhoto': function () {
		$crop.croppie('result', 'base64').then(function(base64) {
			let fsFile = new FS.File(base64);
			ProfileImages.insert(fsFile, function (err, result) {
				if (err){
					console.log(err);
				} else{
					console.log(result);
					let path = '/cfs/files/ProfileImages/' + result._id;
					Meteor.call('updateUserPhotoMethod', path);
				}
			})
		});
	}
})*/
