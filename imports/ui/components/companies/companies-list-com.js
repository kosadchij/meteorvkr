import './companies-list-com.html';
import { Meteor } from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {Companies} from '../../../api/companies/index.js';

Template.companiesListCom.onCreated(function () {
    this.subscribe('companies.all');
});

Template.companiesListCom.helpers({
    companies() {
        let loggedInUser = Meteor.user();
        if (typeof loggedInUser != 'undefined')
            return Companies.find();
    }
});
