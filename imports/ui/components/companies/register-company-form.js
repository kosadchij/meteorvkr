import './register-company-form.html';
import '../common/autoforms/schema.js';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {Template} from 'meteor/templating';

Template.registerCompanyForm.helpers({
  registerCompanySchema: function() {
    return Schema.registerCompany;
  }
});

Template.registerCompanyForm.onRendered(function() {
	let registerCompanyFormHooks = {

		// Called when any submit operation succeeds
		onSuccess: function(formType, result) {
			toastr['success'](TAPi18n.__('Company successfully created'), 'Success');
            let login = $('#loginField').val();
            let password = $('#passwordField').val();
            Meteor.loginWithPassword(login, password, function(e) {
                if (e){
                    Command: toastr['error'](TAPi18n.__(e.reason), 'Authentication error');
                    console.log(e.reason);
                } else {
                    FlowRouter.go('App.usersList');
                }
            });
		},

		// Called when any submit operation fails
		onError: function(formType, error) {
			toastr['error'](error.reason, 'Error');
		},
	};
	AutoForm.addHooks('registerCompanyForm', registerCompanyFormHooks, true)
});
