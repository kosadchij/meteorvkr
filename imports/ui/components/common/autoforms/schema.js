import {Tracker} from 'meteor/tracker';
import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

Schema = {};
Schema.updateUserPhoto = new SimpleSchema({
    photo: {
        type: String
    }
});
Schema.addUser = new SimpleSchema({
    /*name: {
		type: String,
		label: "Your name",
		max: 50
	},*/
	email: {
		type: String,
		regEx: SimpleSchema.RegEx.Email,
		label: "E-mail address"
	},
	/*password: {
		type: String,
		label: "Enter a password",
		min: 8,
	},
	confirmPassword: {
		type: String,
		label: "Enter the password again",
		min: 8,
		custom() {
			if (this.value !== this.field('password').value) {
				return "passwordMismatch";
			}
		},
	},*/
	roles: {
		type: Array,
		label: "Roles",
		autoform: {
			type: 'select-checkbox',
			options: [
				{
					label: 'Диспетчер',
					value: 'dispatcher'
				},
				{
					label: 'Админ',
					value: 'admin'
				}
			]
		}
	},
	'roles.$': {
        optional: true,
		type: String,
		allowedValues: ['admin', 'dispatcher']
	}
}, { tracker: Tracker });

Schema.updateUser = new SimpleSchema({
    userId: {
		type: String,
		max: 50
	},
    name: {
		type: String,
		label: "Full name",
		max: 50
	},
	email: {
		type: String,
		regEx: SimpleSchema.RegEx.Email,
		label: "E-mail address"
	},
	workPhone: {
        optional: true,
		type: String,
		regEx: SimpleSchema.RegEx.Phone,
		label: "Work phone"
	},
	phone: {
        optional: true,
		type: String,
		regEx: SimpleSchema.RegEx.Phone,
		label: "Mobile phone"
	},
	skype: {
        optional: true,
		type: String,
		label: "Skype",
        max: 50
	},
	position: {
        optional: true,
		type: String,
		label: "Position",
        max: 50
	},
	office: {
        optional: true,
		type: String,
		label: "Office location",
        max: 50
	},
	otherInfo: {
        optional: true,
		type: String,
		label: "About you",
        max: 500
	}
}, { tracker: Tracker });

Schema.registerCompany = new SimpleSchema({
    companyName: {
		type: String,
		label: "Company name",
		max: 50
	},
    name: {
		type: String,
		label: "Your name",
		max: 50
	},
    INN: {
		type: Number,
		label: "INN",
		max: 9999999999999
	},
    KPP: {
		type: Number,
		label: "KPP",
		max: 9999999999999
	},
	email: {
		type: String,
		regEx: SimpleSchema.RegEx.Email,
		label: "Your E-mail address"
	},
	password: {
		type: String,
		label: "Enter a password",
		min: 8,
	},
	confirmPassword: {
		type: String,
		label: "Enter the password again",
		min: 8,
		custom() {
			if (this.value !== this.field('password').value) {
				return "passwordMismatch";
			}
		},
	}
}, { tracker: Tracker });

Schema.addContainer = new SimpleSchema({
    feet: {
		type: Number,
		label: "Feet",
		max: 40
	},
    isNew: {
    	type: Boolean,
        label: "New",
    	defaultValue: false,
    },
    fridge: {
    	type: Boolean,
        label: "Refrigerator",
    	defaultValue: false,
    },
    height: {
		type: Number,
		label: "Height, mm",
		max: 10000
	},
    length: {
		type: Number,
		label: "Length, mm",
		max: 100000
	},
    width: {
		type: Number,
		label: "Width, mm",
		max: 100000
	},
    state: {
		type: String,
		label: "State",
		max: 50
	},
    location: {
		type: String,
		label: "Location",
		max: 50
	},
    price: {
		type: Number,
		label: "Price, RUB./day"
	},
    description: {
		type: String,
		label: "Description"
	}
}, { tracker: Tracker });

Schema.updateContainer = new SimpleSchema({
    containerId: {
		type: String
	},
    feet: {
		type: Number,
		label: "Feet",
		max: 40
	},
    isNew: {
    	type: Boolean,
        label: "New",
    	defaultValue: false,
    },
    fridge: {
    	type: Boolean,
        label: "Refrigerator",
    	defaultValue: false,
    },
    height: {
		type: Number,
		label: "Height, mm",
		max: 10000
	},
    length: {
		type: Number,
		label: "Length, mm",
		max: 100000
	},
    width: {
		type: Number,
		label: "Width, mm",
		max: 100000
	},
    state: {
		type: String,
		label: "State",
		max: 50
	},
    location: {
		type: String,
		label: "Location",
		max: 50
	},
    price: {
		type: Number,
		label: "Price, RUB./day"
	},
    description: {
		type: String,
		label: "Description"
	}
}, { tracker: Tracker });
