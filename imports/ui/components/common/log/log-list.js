import './log-list.html';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {Template} from 'meteor/templating';
import {AppLogs} from '../../../../api/log/index.js';
import {moment} from 'meteor/momentjs:moment';
import {ReactiveVar} from 'meteor/reactive-var';

Template.logList.onCreated( () =>  {
    let instance = Template.instance();
    instance.autorun( () => {
        if (instance.data.date){
            console.log(instance.data.date);
            instance.subscribe('log.messages', instance.data.date.get());
        }
    })
});

Template.logList.helpers({
    messages() {
        let loggedInUser = Meteor.user();
        if (typeof loggedInUser != 'undefined')
            return AppLogs.find();
    },
    isEq(level1, level2){
        return level1 === level2;
    },
    getMessageDate(date){
        return moment(date).format('YYYY-MM-DD HH:mm');
    },
    getMessageOwner(userId){
        return Meteor.users.findOne({_id: userId}).profile.name;
    },
});
