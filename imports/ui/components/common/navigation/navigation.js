import './navigation.html';
import {Template} from 'meteor/templating';
import {ProfileImages} from '../../../../api/users/index.js';

Template.navigation.rendered = function(){

    // Initialize metisMenu
    $('#side-menu').metisMenu();

};

Template.navigation.onCreated(function() {
	this.subscribe('profileImages.all');
});

Template.navigation.helpers({

    getUserInfo(){
        let user = Meteor.user();
        if (user){
            return {
                userId: user._id,
                name: user.profile.name,
                position: user.profile.position,
                company: user.profile.company,
                profileImage: user.profile.profileImage
            };
        }
    },

});

// Used only on OffCanvas layout
Template.navigation.events({

    'click .close-canvas-menu' : function(){
        $('body').toggleClass("mini-navbar");
    },
    'change #profileimg': function() {
		let input = $('#profileimg');
		if (input.prop('files') && input[0].files[0]) {
			let reader = new FileReader();

			reader.onload = function(e) {
                if ( $('.cr-boundary').length > 0 )
                    $('.cr-boundary').remove();
                    $('.cr-slider-wrap').remove();

				$('#blah').attr('src', e.target.result);
				$crop = $('#blah').croppie({
					enableExif: true,
					viewport: {
						width: 200,
						height: 200,
						type: 'circle'
					},
					boundary: {
						width: 300,
						height: 300
					}
				});
			}

			reader.readAsDataURL(input[0].files[0]);
		}
	},
    'click #uploadPhoto': function () {
		$crop.croppie('result', 'base64').then(function(base64) {
			let fsFile = new FS.File(base64);
            let user = Meteor.user();
            if (user.profile.profileImage){
                let oldFileId = user.profile.profileImage.split('/')[4]
                let oldFile = ProfileImages.findOne({_id: oldFileId});
                oldFile.remove();
            }
			ProfileImages.insert(fsFile, (err, result) => {
				if (err){
					throw new Meteor.Error(500, err.reason);
				} else{
					let path = '/cfs/files/ProfileImages/' + result._id;
                    Meteor.call('updateUserPhotoMethod', path);
				}
			});
		});
	}

});
