import './containers-update-com.html';
import '../common/autoforms/schema.js';
import { Meteor } from 'meteor/meteor';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {Template} from 'meteor/templating';
import {Containers} from '../../../api/containers/index.js';

Template.updateContainerForm.onCreated(function () {
    this.subscribe('containers.one', this.data.containerId);
});

Template.updateContainerForm.helpers({
    getContainerId(){
        return this.containerId;
    },
    updateContainerFormSchema(){
      return Schema.updateContainer;
    },
    getContainerInfo() {
		let container = Containers.findOne({_id: this.containerId});
        if(container){
            return {
                containerId: container._id,
                feet: container.feet,
    			height: container.height,
    			length: container.length,
    			width: container.width,
    			price: container.price,
    			description: container.description,
    			isNew: container.isNew,
    			fridge: container.fridge,
    			state: container.state,
    			location: container.location,
            };
        }
	},
});

Template.updateContainerForm.onRendered(function() {
	let updateContainerFormHooks = {

		// Called when any submit operation succeeds
		onSuccess: function(formType, result) {
			toastr['success'](TAPi18n.__('Container successfully updated'), 'Success');
			FlowRouter.go('/containers/view/' + FlowRouter.getParam('conId'));
		},

		// Called when any submit operation fails
		onError: function(formType, error) {
			toastr['error'](error.reason, 'Error');
		},
	};
	AutoForm.addHooks('updateContainerForm', updateContainerFormHooks, true)
});
