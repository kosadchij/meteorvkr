import './containers-list-all-com.html';
import { Meteor } from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {Containers} from '../../../api/containers/index.js';

Template.containersListAllCom.onCreated(function () {
    this.subscribe('containers.all');
});

Template.containersListAllCom.helpers({
    containers() {
        let loggedInUser = Meteor.user();
        if (typeof loggedInUser != 'undefined')
            return Containers.find();
    }
});
