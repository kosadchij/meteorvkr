import './containers-add-com.html';
import '../common/autoforms/schema.js';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {Template} from 'meteor/templating';

Template.addContainerForm.helpers({
	addContainerFormSchema: function() {
		return Schema.addContainer;
	},
    feetOptions: function() {
        return [{
        		label: "20",
        		value: 20
        	},
        	{
        		label: "30",
        		value: 30
        	},
        	{
        		label: "40",
        		value: 40
        	}
        ];
    },
    stateOptions: function() {
        return [{
        		label: "Excellent",
        		value: "Excellent"
        	},
        	{
        		label: "Good",
        		value: "Good"
        	},
        	{
        		label: "Normal",
        		value: "Normal"
        	},
        	{
        		label: "Bad",
        		value: "Bad"
        	},
        ];
    },
});

Template.addContainerForm.onRendered(function() {
	let addContainerFormHooks = {

		// Called when any submit operation succeeds
		onSuccess: function(formType, result) {
			toastr['success'](TAPi18n.__('Container successfully added'), 'Success');
			FlowRouter.go('App.usersList');
		},

		// Called when any submit operation fails
		onError: function(formType, error) {
			toastr['error'](error.reason, 'Error');
		},
	};
	AutoForm.addHooks('addContainerForm', addContainerFormHooks, true)
});
