import './containers-list-company-com.html';
import { Meteor } from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {Containers} from '../../../api/containers/index.js';

Template.containersListCompanyCom.onCreated(function () {
    this.subscribe('containers.company');
});

Template.containersListCompanyCom.helpers({
    containers() {
        let loggedInUser = Meteor.user();
        if (typeof loggedInUser != 'undefined')
            return Containers.find();
    }
});
