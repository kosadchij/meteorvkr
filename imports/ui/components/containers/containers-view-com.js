import './containers-view-com.html';
import {Template} from 'meteor/templating';
import {Mongo} from 'meteor/mongo';
import {Companies} from '../../../api/companies/index.js';
import {Containers} from '../../../api/containers/index.js';

Template.containersViewCom.onCreated(function() {
	this.subscribe('containers.one', this.data.containerId, {
		onReady: function () {
			setTimeout(	function() {
				$('.product-images').slick({
					dots: true
				});
			}, 1);
		}
	});
});

Template.containersViewCom.onRendered(function () {
    $('.input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    })
});

Template.containersViewCom.helpers({
	getContainerId() {
		return this.containerId;
	},
	getDate(){
        return moment().format('YYYY-MM-DD');
    },
	getContainerInfo() {
		let container = Containers.findOne({_id: this.containerId});
		return {
			feet: container.feet,
			height: container.height,
			length: container.length,
			width: container.width,
			price: container.price,
			description: container.description,
			isNew: container.isNew,
			fridge: container.fridge,
			state: container.state,
			location: container.location,

			companyName: Companies.findOne({
				_id: container.company
			}).name
		};
	},
	isContainerOwner(){
		let container = Containers.findOne({_id: this.containerId});
		let company = Companies.findOne({_id: container.company});
		let loggedInUser = Meteor.user();
		if (loggedInUser){
			if (loggedInUser.profile.company == company._id){
				return true;
			}
		}
	}
});

Template.containersViewCom.events({
	'click #rentContainer': function () {
		let unindexed_array = $('#containerRentForm').serializeArray();
		let indexed_array = {};

		$.map(unindexed_array, function(n, i){
			indexed_array[n['name']] = n['value'];
		});

		indexed_array.containerId = this.containerId;
		console.log(indexed_array);
		Meteor.call('rentContainerMethod', indexed_array,
					function (error, result) {
			if (error){
				toastr['error'](error.reason, 'Error');
			} else {
				toastr['success'](TAPi18n.__('Container successfully rented'), 'Success');
				$('#containerRentForm').modal('toggle');
			}
		})
	}
});
