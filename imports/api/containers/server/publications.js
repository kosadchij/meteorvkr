import { Meteor } from 'meteor/meteor';
import {Roles} from 'meteor/alanning:roles';
import {Containers} from '../index.js';
import {Companies} from '../../companies/index.js';

Meteor.publish('containers.all', function () {
    var loggedInUser = Meteor.users.findOne(this.userId);
    if (!loggedInUser) {
        throw new Meteor.Error(403, "Access denied");
    }
    return Containers.find();
});

Meteor.publish('containers.company', function () {
    var loggedInUser = Meteor.users.findOne(this.userId);
    if (!loggedInUser) {
        throw new Meteor.Error(403, "Access denied");
    }
    return Containers.find({company: loggedInUser.profile.company});
});

Meteor.publish('containers.one', function (conId) {
    let loggedInUser = Meteor.users.findOne(this.userId);
    if (!loggedInUser) {
        throw new Meteor.Error(403, "Access denied");
    }

    let container = Containers.find({_id: conId});

    let companyIds = [];
    container.map(function (container) {
        companyIds.push(container.company);
    });

    let company = Companies.find({_id: {$in: companyIds}})

    return [container, company];
});
