import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import { Email } from 'meteor/email'
import {Roles} from 'meteor/alanning:roles';
import {ContainerImages} from '../index.js';
import {Containers} from '../index.js';
import {log} from '../../log/index.js';
import '../../../ui/components/common/autoforms/schema.js';

Meteor.methods({
	addContainerMethod: function(data) {

		Schema.addContainer.validate(data);

		let loggedInUser = Meteor.user();
		if (!loggedInUser &&
            (!Roles.userIsInRole(loggedInUser, ['admin']) || !Roles.userIsInRole(loggedInUser, ['dispatcher']))) {
			throw new Meteor.Error(403, "Access denied");
		}

        Containers.insert({
            feet: data.feet,
            isNew: data.isNew,
            fridge: data.fridge,
            height: data.height,
            length: data.length,
            width: data.width,
            state: data.state,
            location: data.location,
            price: data.price,
            description: data.description,
            company: loggedInUser.profile.company,
            author: loggedInUser._id,
            createdAt: new Date()
        });
		log.info('Container ' + data.containerId + ' sucsessfully added', data, this.userId);
  	},
	updateContainerMethod: function(data) {

		Schema.updateContainer.validate(data);

		let loggedInUser = Meteor.user();
		if (!loggedInUser &&
            (!Roles.userIsInRole(loggedInUser, ['admin']) || !Roles.userIsInRole(loggedInUser, ['dispatcher']))) {
			throw new Meteor.Error(403, "Access denied");
		}

        Containers.update({_id: data.containerId}, {$set: {
            'feet': data.feet,
            'isNew': data.isNew,
            'fridge': data.fridge,
            'height': data.height,
            'length': data.length,
            'width': data.width,
            'state': data.state,
            'location': data.location,
            'price': data.price,
            'description': data.description,
            'updatedAt': new Date()
        }});
		log.info('Container ' + data.containerId + ' sucsessfully updated', data, this.userId);
  	},
	rentContainerMethod: function(data) {
		console.log(data);
		let loggedInUser = Meteor.user();
		if (!loggedInUser &&
            (!Roles.userIsInRole(loggedInUser, ['admin']) || !Roles.userIsInRole(loggedInUser, ['dispatcher']))) {
			throw new Meteor.Error(403, "Access denied");
		}

		Containers.update({_id: data.containerId}, {$set: {
            'rent.company': loggedInUser.profile.company,
            'rent.from': data.rentCompanyFrom,
            'rent.to': data.rentCompanyTo,
            'rent.confirm': false,
            'updatedAt': new Date()
        }});

		log.info('Container ' + data.containerId + ' asked to lease by ' + loggedInUser.profile.name + '(' + data.companyName + ')', data, this.userId);
	}
});
