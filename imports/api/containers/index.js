import { Mongo } from 'meteor/mongo';

export const Containers = new Mongo.Collection('Containers');
export const ContainerImages = new FS.Collection('ContainerImages', {
    stores: [new FS.Store.GridFS('ContainerImages')]
});

ContainerImages.allow({
    insert: function(userId, doc) {
        return true;
    },
    update: function(userId, doc, fields, modifier) {
        return true;
    },
    download: function() {
        return true;
    },
    remove: function() {
        return true;
    }
});
