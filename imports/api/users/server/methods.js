import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import { Email } from 'meteor/email'
import {Roles} from 'meteor/alanning:roles';
import {ProfileImages} from '../index.js';
import {log} from '../../log/index.js';
import '../../../ui/components/common/autoforms/schema.js';

Meteor.methods({
	addUserMethod: function(data) {
		Schema.addUser.validate(data);
		let loggedInUser = Meteor.user();
		if (!loggedInUser ||
			!Roles.userIsInRole(loggedInUser, ['admin'])) {
			throw new Meteor.Error(403, "Access denied");
		}
		if(Meteor.users.findOne({'emails.address': data.email})){
			throw new Meteor.Error(500, "User is already registered");
		}

		let password = (function() {
			let text = "";
			let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			for (let i = 0; i < 8; i++)
				text += possible.charAt(Math.floor(Math.random() * possible.length));

			return text;
		}());

		let id;
		id = Accounts.createUser({
			email: data.email,
			password: password,
			profile: {
				name: data.email,
				company: loggedInUser.profile.company
			}
		});

		if (data.roles.length > 0){
			Roles.addUsersToRoles(id, data.roles);
		}

		data.type = 'users';
		data.company = loggedInUser.profile.company;

		log.info('User ' + data.email + ' sucsessfully added', data, this.userId);

		Meteor.defer(function () {
			Email.send({
				from: "from@mailinator.com",
				to: data.email,
				subject: "You are invited",
				text: "your pass: " + password
			});
		});
  	},
	updateUserMethod(data){
		Schema.updateUser.validate(data);
		let loggedInUser = Meteor.user();
		if (!loggedInUser) {
			throw new Meteor.Error(403, "Access denied");
		}
		Meteor.users.update({_id: data.userId}, {$set: {
			"emails.0.address": data.email,
			"profile.name": data.name,
			"profile.workPhone": data.workPhone,
			"profile.phone": data.phone,
			"profile.skype": data.skype,
			"profile.position": data.position,
			"profile.office": data.office,
			"profile.otherInfo": data.otherInfo
		}});
		data.type = 'users';
		log.info('User ' + data.email + ' sucsessfully updated', data, this.userId);
	},
	updateUserPhotoMethod(path){
		let loggedInUser = Meteor.user();
		if (!loggedInUser) {
			throw new Meteor.Error(403, "Access denied");
		}
		Meteor.users.update({_id: loggedInUser._id}, {$set: {
			"profile.profileImage": path
		}})
	}
});
