import { Meteor } from 'meteor/meteor';
import {Roles} from 'meteor/alanning:roles';
import {Companies} from '../../companies/index.js';
import {ProfileImages} from '../index.js';

Meteor.publish('profileImages.all', function () {
    let loggedInUser = Meteor.users.findOne(this.userId);
    if (!loggedInUser) {
        throw new Meteor.Error(403, "Access denied");
    }
    let profileImages = ProfileImages.find();
    return profileImages;
});
Meteor.publish('users.all', function () {
    let loggedInUser = Meteor.users.findOne(this.userId);
    if (!loggedInUser) {
        throw new Meteor.Error(403, "Access denied");
    }

    let users = Meteor.users
                      .find({ "profile.company":loggedInUser.profile.company });
    let companyIds = [];
    users.map(function (user) {
        companyIds.push(user.profile.company);
    })
    let userIds = [];
    users.map(function (user) {
        userIds.push(user._id);
    })

    return [users, Companies.find(), ProfileImages.find({userId: {$in: userIds}})];
});

Meteor.publish('users.one', function (uId) {
    let loggedInUser = Meteor.users.findOne(this.userId);
    if (!loggedInUser) {
        throw new Meteor.Error(403, "Access denied");
    }

    let user = Meteor.users.find({_id: uId});
    let companyIds = [];
    user.map(function (user) {
        companyIds.push(user.profile.company);
    })
    let userIds = [];
    user.map(function (user) {
        userIds.push(user._id);
    })

    return [user, Companies.find({_id: {$in: companyIds}}), ProfileImages.find({userId: {$in: userIds}})];
});
