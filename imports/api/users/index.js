import { Mongo } from 'meteor/mongo';

export const ProfileImages = new FS.Collection('ProfileImages', {
    stores: [new FS.Store.GridFS('ProfileImages')]
});

ProfileImages.allow({
    insert: function(userId, doc) {
        return true;
    },
    update: function(userId, doc, fields, modifier) {
        return true;
    },
    download: function() {
        return true;
    },
    remove: function() {
        return true;
    }
});
