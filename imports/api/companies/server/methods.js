import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import { Email } from 'meteor/email'
import {Roles} from 'meteor/alanning:roles';
import {Companies} from '../index.js';
import '../../../ui/components/common/autoforms/schema.js';


Meteor.methods({
	registerCompanyMethod: function(data) {
		Schema.registerCompany.validate(data);
		let loggedInUser = Meteor.user();

        if(Companies.findOne({'INN': data.INN, 'KPP': data.KPP})){
            throw new Meteor.Error(500, "Company is already registered");
        }

		if (loggedInUser) {
			throw new Meteor.Error(403, "You are already registered");
		}

        let companyId = Companies.insert({
            name: data.companyName,
            INN: data.INN,
            KPP: data.KPP,
            createdAt: new Date()
        });

		let id;
		id = Accounts.createUser({
			email: data.email,
			password: data.password,
			profile: {
				name: data.name,
				company: companyId
			}
		});

        Roles.addUsersToRoles(id, 'admin');

		Meteor.defer(function () {
			Email.send({
				from: "from@mailinator.com",
				to: data.email,
				subject: "You are invited",
				text: "your pass: " + data.password,
			});
		});
  	}
});
