import { Meteor } from 'meteor/meteor';
import {Roles} from 'meteor/alanning:roles';
import {Companies} from '../index.js';
import {Containers} from '../../containers/index.js';

Meteor.publish('companies.all', function () {
    var loggedInUser = Meteor.users.findOne(this.userId);
    if (!loggedInUser) {
        throw new Meteor.Error(403, "Access denied");
    }
    return Companies.find();
});

Meteor.publish('companies.one', function (cId) {
    let loggedInUser = Meteor.users.findOne(this.userId);
    if (!loggedInUser) {
        throw new Meteor.Error(403, "Access denied");
    }

    let company = Companies.find({_id: cId});
    let users = Meteor.users.find({"profile.company": cId});
    let containers = Containers.find({"company": cId});

    return [company, users, containers];
});
