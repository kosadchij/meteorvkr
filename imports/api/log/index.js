import { Logger }      from 'meteor/ostrio:logger';
import { LoggerMongo } from 'meteor/ostrio:loggermongo';

// Initialize Logger:
export const log = new Logger();
export const AppLogs = new Mongo.Collection('AppLogs');

// Initialize LoggerMongo with collection instance:
const LogMongo = new LoggerMongo(log, {
  collection: AppLogs
});

// Enable LoggerMongo with default settings:
LogMongo.enable();
