import { AppLogs } from '../index.js';
import {moment} from 'meteor/momentjs:moment';

Meteor.publish('log.messages', function (date) {
    let loggedInUser = Meteor.users.findOne(this.userId);
    let logs = AppLogs.find({
        $and: [
            {'additional.company': loggedInUser.profile.company},
            {'date': {$gt: moment.utc(date).startOf('day').toDate()}},
            {'date': {$lt: moment.utc(date).startOf('day').add(1, 'days').toDate()}}
        ]
    });

    let ownerIds = [];
    logs.map(function(log) {
    	ownerIds.push(log.userId);
    })
    return [logs, Meteor.users.find({_id: {in: ownerIds}})];
});
